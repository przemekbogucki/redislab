#REDIS - PowerShall version

#---- Rubby on windows

#install rubyinstaller-2.6.1-1-x64.exe
#run powershall
gem install redis
.\redis-trib.rb

#REDIS - PowerShall version
#---- BUILD Docker -------------------------

Docker build -t redis-image .

#---- set ENVIRONMENT parameters -----------
ipconfig
# find virtunalBox IP
$REDIS_IP="<IP>"
$REDIS_IP="172.28.64.1"
$REDIS_NODE1=$REDIS_IP+':7001'
$REDIS_NODE2=$REDIS_IP+':7002'
$REDIS_NODE3=$REDIS_IP+':7003'
$REDIS_NODE4=$REDIS_IP+':7004'
$REDIS_NODE5=$REDIS_IP+':7005'
$REDIS_NODE6=$REDIS_IP+':7006'

---- Run dockers with Redis nodes ---------
docker run --name redis1 --hostname redis1 -e IP=$REDIS_IP -e PORT='7001' -e BPORT='17001' -p 7001:7001 -p 17001:17001 -d redis-image 
docker run --name redis2 --hostname redis2 -e IP=$REDIS_IP -e PORT='7002' -e BPORT='17002' -p 7002:7002 -p 17002:17002 -d redis-image 
docker run --name redis3 --hostname redis3 -e IP=$REDIS_IP -e PORT='7003' -e BPORT='17003' -p 7003:7003 -p 17003:17003 -d redis-image
docker run --name redis4 --hostname redis4 -e IP=$REDIS_IP -e PORT='7004' -e BPORT='17004' -p 7004:7004 -p 17004:17004 -d redis-image
docker run --name redis5 --hostname redis5 -e IP=$REDIS_IP -e PORT='7005' -e BPORT='17005' -p 7005:7005 -p 17005:17005 -d redis-image
docker run --name redis6 --hostname redis6 -e IP=$REDIS_IP -e PORT='7006' -e BPORT='17006' -p 7006:7006 -p 17006:17006 -d redis-image

#---- Create and check REDIS Cluster -----------------
./redis-trib.rb create --replicas 1 $REDIS_NODE1 $REDIS_NODE2 $REDIS_NODE3 $REDIS_NODE4 $REDIS_NODE5 $REDIS_NODE6

./redis-trib.rb check $REDIS_NODE1
./redis-trib.rb info $REDIS_POINT1
