package com.pb.edu;

import com.pb.edu.Redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.RedisClusterCommands;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Map;

@SpringBootApplication
@ComponentScan(excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = {PushApplication.class, SingleKeyPushApplication.class})})
public class RedisPartitionsClient implements CommandLineRunner {
    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    @Lazy
    @Autowired
    StringRedisTemplate redisTemplate;

    private static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) {
        try {
            new SpringApplicationBuilder(RedisPartitionsClient.class)
                    .web(WebApplicationType.NONE) // .REACTIVE, .SERVLET
                    .run(args);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            ApplicationConfiguration.getConfigurationHint();
        }
    }

    @Override
    public void run(String... args) throws Exception {
        querySingleKeyData();
    }

    private void querySingleKeyData() {
        RedisClusterCommands clusterCommands = redisTemplate.getConnectionFactory().getClusterConnection();
        Map<Integer, String> partitions = RedisUtils.getSingleKeyPerPartition(clusterCommands);

        for (int i = 0; i < applicationConfiguration.getIterations() || applicationConfiguration.getIterations() == 0; i++) {
            try {
                for (Integer pid : partitions.keySet()) {
                    String key = partitions.get(pid);
                    String value = redisTemplate.opsForValue().get(key);
                    System.out.println("get Key:" + key + " Result:" + " value:" + value);
                }
                System.out.println();
                Thread.sleep(applicationConfiguration.getSleepTimeInSec());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }


}
