package com.pb.edu;

import com.pb.edu.Redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.RedisClusterCommands;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Map;
import java.util.Random;

@SpringBootApplication
@ComponentScan(excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = {PushApplication.class, RedisPartitionsClient.class})})
public class SingleKeyPushApplication implements CommandLineRunner {

    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    @Lazy
    @Autowired
    StringRedisTemplate redisTemplate;

    private static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) {
        try {
            new SpringApplicationBuilder(SingleKeyPushApplication.class)
                    .web(WebApplicationType.NONE) // .REACTIVE, .SERVLET
                    .run(args);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            ApplicationConfiguration.getConfigurationHint();
        }
    }

    @Override
    public void run(String... args) throws Exception {
        RedisUtils.redisClusterFlush(redisTemplate);
        pushData();
    }


    private void pushData() {
        RedisClusterCommands clusterCommands = redisTemplate.getConnectionFactory().getClusterConnection();
        Map<Integer, String> partitions = RedisUtils.getSingleKeyPerPartition(clusterCommands);
        Random rd = new Random();

        for (int i = 0; i < applicationConfiguration.getIterations() || applicationConfiguration.getIterations() == 0; i++) {
            try {
                for (Integer pid : partitions.keySet()) {
                    String key = partitions.get(pid);
                    String value = Integer.toString(rd.nextInt(100));
                    redisTemplate.opsForValue().set(key, value);
                    System.out.println("added Key:" + partitions.get(pid) + " value:" + value + " location: " + RedisUtils.getClusterOperationDetails(clusterCommands, key));
                }
                System.out.println();
                Thread.sleep(applicationConfiguration.getSleepTimeInSec());
            } catch (Throwable e) {
                e.printStackTrace();
            }

        }
    }

}
