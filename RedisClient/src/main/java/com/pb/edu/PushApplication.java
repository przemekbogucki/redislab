package com.pb.edu;

import com.pb.edu.Redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.RedisClusterCommands;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Random;

@SpringBootApplication
@ComponentScan(excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = {SingleKeyPushApplication.class, RedisPartitionsClient.class})})
public class PushApplication implements CommandLineRunner {


    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    @Lazy
    @Autowired
    StringRedisTemplate redisTemplate;

    private static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) {
        try {
            new SpringApplicationBuilder(PushApplication.class)
                    .web(WebApplicationType.NONE) // .REACTIVE, .SERVLET
                    .run(args);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            ApplicationConfiguration.getConfigurationHint();
        }
    }

    @Override
    public void run(String... args) throws Exception {
        RedisUtils.redisClusterFlush(redisTemplate);
        pushData();
    }


    private void pushData() {
        RedisClusterCommands clusterCommands = redisTemplate.getConnectionFactory().getClusterConnection();
        Random rd = new Random();
        for (int i = 0; i < applicationConfiguration.getIterations() || applicationConfiguration.getIterations() == 0; i++) {
            try {
                String key = "k" + i;
                String value = Integer.toString(rd.nextInt(100));
                redisTemplate.opsForValue().set(key, value);
                System.out.println("added Key:" + key + " value:" + value + " location: " + RedisUtils.getClusterOperationDetails(clusterCommands, key));
                Thread.sleep(applicationConfiguration.getSleepTimeInSec());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}
