package com.pb.edu.Redis;

import org.springframework.data.redis.connection.RedisClusterCommands;
import org.springframework.data.redis.connection.RedisClusterNode;
import org.springframework.data.redis.core.ClusterOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.Map;

public class RedisUtils {

    public static void redisClusterFlush(StringRedisTemplate redisTemplate) {
        ClusterOperations<String, String> clusterOperations = redisTemplate.opsForCluster();
        System.out.println("Cluster clean up initiated");
        Iterable<RedisClusterNode> clusters = redisTemplate.getConnectionFactory().getClusterConnection().clusterGetNodes();
        for (RedisClusterNode clusterNode : clusters) {
            System.out.println("Node:" + clusterNode);
            if (clusterNode.isMaster()) {
                System.out.println("Node is master, flush it");
                redisTemplate.opsForCluster().flushDb(clusterNode);
            }
        }
    }


    public static Map<Integer, String> getSingleKeyPerPartition(RedisClusterCommands clusterCommands) {
        Map<Integer, String> keysPerPartition = new HashMap<>();
        long numberOfSlots = clusterCommands.clusterGetClusterInfo().getSlotsAssigned();
        for (RedisClusterNode node : clusterCommands.clusterGetNodes()) {
            if (node.isConnected() && !node.isMarkedAsFail() && node.isMaster()) {
                RedisClusterNode.SlotRange slotRange = node.getSlotRange();
                int sizeOfSlotRange = slotRange.getSlotsArray().length;
                int numberOfPartitions = (int) Math.round((double) numberOfSlots / sizeOfSlotRange);

                int i = 1;
                while (keysPerPartition.size() < numberOfPartitions) {
                    String key = "key" + i;
                    Integer pn = getPartitionNumber(clusterCommands, key);
                    if (!keysPerPartition.containsKey(pn)) {
                        keysPerPartition.put(pn, key);
                    }
                    i++;
                }
                return keysPerPartition;
            }
        }
        return keysPerPartition;
    }

    public static int getPartitionNumber(RedisClusterCommands clusterCommands, String key) {
        int slot = clusterCommands.clusterGetSlotForKey(key.getBytes());
        RedisClusterNode node = clusterCommands.clusterGetNodeForSlot(slot);
        RedisClusterNode.SlotRange slotRange = node.getSlotRange();
        int sizeOfSlotRange = slotRange.getSlotsArray().length;
        int numberOfNode = slot / sizeOfSlotRange + 1;

        return numberOfNode;
    }

    public static String getClusterOperationDetails(RedisClusterCommands clusterCommands, String key) {
        int slot = clusterCommands.clusterGetSlotForKey(key.getBytes());
        RedisClusterNode node = clusterCommands.clusterGetNodeForSlot(slot);
        int numberOfNode = getPartitionNumber(clusterCommands, key);

        return "slot:" + slot + " partition:" + numberOfNode + " address:" + node.getHost() + ":" + node.getPort();
    }


}
